import React from 'react';
import "../../style/home/blueComponent.scss";
import Navbar from "./navbar";
import {Link} from "react-router-dom";

function BlueComponent(props) {
    return (
        <div className="white-content-background">
            <div className="blue-content">
                <Navbar/>
                <div className="main-content">
                    <div className="row">
                        <div className="col-md-6">
                            <h1>Biz bilan kelajakni
                                <br/> yarating</h1>
                            <div className="red-line"></div>
                            <p>Biz brendlarga yordam berish uchun tasavvur va <br/>
                                texnologiyani birlashtiramiz. </p>
                            <span className="animation-button">
                                <Link to="/contact" className="button-text d-flex align-items-center text-decoration-none">
                                    <span>Biz bilan aloqa qiling</span>
                                    <button>
                                        <img src="/images/arrow-white-double.svg" alt="no images"/>
                                    </button>
                                </Link>
                            </span>
                        </div>
                        <div className="col-md-6">
                            <div className="bg-images">
                                <div className="dark-blue-circle"></div>
                                <div className="blue-circle"></div>
                                <div className="white-border"></div>
                                <div className="images">
                                    <img
                                        src="https://www.bluefountainmedia.com/sites/default/files/styles/homepage_hero_bottom_470x470_/public/2019-03/homepage_banner_main_1.webp?itok=gH2iICfM"
                                        alt="no images"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BlueComponent;