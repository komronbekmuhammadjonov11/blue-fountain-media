import React from 'react';
import "../../style/home/portfolio.scss";
function Portfolio(props) {
    return (
        <div className="portfolio">
            <div className="left-img">
                <img src="/images/dots-arrow-left-side.png" alt="no images"/>
            </div>
            <div className="img-bottom">
                <img src="/images/dots-arrow-left-side.png" alt="no images"/>
            </div>
            <div className="brand">
                Portfolio
            </div>
            <div className="red-line"></div>
            <h1>Biz bajargan ishlar</h1>
            <div className="row">
                <div className="col-md-4">
                   <div className="card-content">
                       <div className="dark-now"></div>
                       <div className="card">
                           <div className="img-content">
                               <img
                                   src="https://www.bluefountainmedia.com/sites/default/files/styles/insights_555x390_/public/2020-04/CaseStudy_Baldor_homepage_visual.webp?itok=ppUIJksB"
                                   alt="no images"/>
                           </div>
                           <div className="name">LOGISTICS</div>
                           <div className="red-line"></div>
                           <div className="description">
                               Stop & Stor: A digital upgrade for New York’s Self Storage Leader
                           </div>
                       </div>
                   </div>
                </div>
                <div className="col-md-4">
                    <div className="card-content">
                        <div className="dark-now"></div>
                        <div className="card">
                            <div className="img-content">
                                <img
                                    src="https://www.bluefountainmedia.com/sites/default/files/styles/insights_555x390_/public/2020-09/eCommerce_experience_teaser.webp?itok=k6STxfFH"
                                    alt="no images"/>
                            </div>
                            <div className="name">LOGISTICS</div>
                            <div className="red-line"></div>
                            <div className="description">
                                Stop & Stor: A digital upgrade for New York’s Self Storage Leader
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="card-content">
                        <div className="dark-now"></div>
                        <div className="card">
                            <div className="img-content">
                                <img
                                    src="https://www.bluefountainmedia.com/sites/default/files/styles/insights_555x390_/public/2020-10/CaseStudy_NGC_preview_listing_page_thumbnail.webp?itok=8JovKXdA"
                                    alt="no images"/>
                            </div>
                            <div className="name">LOGISTICS</div>
                            <div className="red-line"></div>
                            <div className="description">
                                Stop & Stor: A digital upgrade for New York’s Self Storage Leader
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="card-content">
                        <div className="dark-now"></div>
                        <div className="card">
                            <div className="img-content">
                                <img
                                    src="https://www.bluefountainmedia.com/sites/default/files/styles/insights_555x390_/public/2020-10/CaseStudy_NGC_preview_listing_page_thumbnail.webp?itok=8JovKXdA"
                                    alt="no images"/>
                            </div>
                            <div className="name">LOGISTICS</div>
                            <div className="red-line"></div>
                            <div className="description">
                                Stop & Stor: A digital upgrade for New York’s Self Storage Leader
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="card-content">
                        <div className="dark-now"></div>
                        <div className="card">
                            <div className="img-content">
                                <img
                                    src="https://www.bluefountainmedia.com/sites/default/files/styles/insights_555x390_/public/2021-03/CaseStudy_Avetta_Thubnail.webp?itok=XxY_ik5n"
                                    alt="no images"/>
                            </div>
                            <div className="name">LOGISTICS</div>
                            <div className="red-line"></div>
                            <div className="description">
                                Stop & Stor: A digital upgrade for New York’s Self Storage Leader
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="card-content">
                        <div className="dark-now"></div>
                        <div className="card">
                            <div className="img-content">
                                <img
                                    src="https://www.bluefountainmedia.com/sites/default/files/styles/insights_555x390_/public/2020-01/CaseStudy_BluePearl_homepage.webp?itok=XoymAazK"
                                    alt="no images"/>
                            </div>
                            <div className="name">LOGISTICS</div>
                            <div className="red-line"></div>
                            <div className="description">
                                Stop & Stor: A digital upgrade for New York’s Self Storage Leader
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Portfolio;