import React from 'react';
import BlueComponent from "./blueComponent";
import Developers from "../company/developers";
import Portfolio from "./portfolio";
import MyComponyPage from "./myComponyPage";
import Footer from "./footer";

function Home(props) {
    React.useEffect(()=>{
        window.scrollTo(0,0);
    });
    return (
        <div className="home">
            <BlueComponent/>
            <Developers/>
            <Portfolio/>
            <MyComponyPage/>
            <Footer/>
        </div>
    );
}

export default Home;