import React, {useState} from 'react';
import {Link} from "react-router-dom";
import "../../style/home/navbar.scss";

function Navbar(props) {
    function scroll() {
        window.addEventListener('scroll', () => {
           if (window.innerWidth>576){
               let navbar = document.getElementById('navbar');
               navbar.classList.toggle("border-bottom", window.scrollY > 0)
           }
        })
    }

    scroll();

    const [check, setCheck]=useState(false);
    function hover(id) {
        if (id===1){
            // document.getElementById('drop').classList.add('is-hover');
            // document.getElementById('drop1').classList.remove('is-hover');
            // document.getElementById('drop2').classList.remove('is-hover')
        }else if (id===2){
            // document.getElementById('drop1').classList.add('is-hover');
            // document.getElementById('drop').classList.remove('is-hover');
            // document.getElementById('drop2').classList.remove('is-hover')
        }else if (id===3){
            document.getElementById('drop2').classList.add('is-hover');
            document.getElementById('drop').classList.remove('is-hover');
            document.getElementById('drop1').classList.remove('is-hover');
        }
    }
    function cencel() {
        let is_hover=document.getElementById('drop');
        let is_hover1=document.getElementById('drop1');
        let is_hover2=document.getElementById('drop2');
        is_hover.classList.remove('is-hover');
        is_hover1.classList.remove('is-hover');
        is_hover2.classList.remove('is-hover');
    }
    return (
        <div className={`navbar-content ${window.location.pathname==="/"&& window.innerWidth>576? "background-white": ""} ${check? "collapse-open-bg": ""}`} id="navbar">
            <div className="navbar navbar-expand-sm navbar-light">
                <div className="navbar-brand">
                    <Link to="/">
                        <img src="/images/logo white.webp" className="logo-white" alt="no image"/>
                        <img src="/images/logo.webp" className="logo media-logo" alt="no image"/>
                    </Link>
                </div>
                <ul className={`navbar-nav ${check? "media-list-block":""}`}>
                    <li className="nav-item " id="drop">
                        <Link to="/services"  className="nav-link d-flex"  onMouseOver={()=>hover(1)}  >
                            Xizmatlar
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/work" className="nav-link">
                            Ish
                        </Link>
                    </li>
                    <li className="nav-item" id="drop1">
                        <Link to="/company/leadership" className="nav-link d-flex" onMouseOver={()=>hover(2)}>
                            Company
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/contact" className="nav-link">
                            Aloqa
                        </Link>
                    </li>
                </ul>
                <div className="navbar-icon d-none" onClick={()=>setCheck(!check)}>
                    <div className={`navbar-icon-content-1 ${check? "d-none": "d-block"}`}>
                        <div className="icon-line"></div>
                        <div className="icon-line"></div>
                        <div className="icon-line"></div>
                    </div>
                    <div className={`navbar-icon-content-2 ${check? "d-block": "d-none"}`}>
                        <div className="icon-line"></div>
                        <div className="icon-line"></div>
                    </div>
                </div>
                <div className="language">
                    <div id="drop2">
                        <div onMouseOver={()=>hover(3)}  className="text language-text text-decoration-none">
                            Tillar
                            <div className="icon">

                            </div>
                        </div>
                        <div className="menu-bar ">
                            <div className="menu-bar-content">
                                <div className="cancel" onClick={cencel}></div>
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="tab-list">
                                            <ul className="list-unstyled">
                                                <li className="active">
                                                    <Link to="/"
                                                          className="d-flex align-items-center text-decoration-none">
                                                        <img src="/images/arrow-bright-long.svg" alt="no images"/>
                                                        English
                                                    </Link>
                                                </li>
                                                <li className="">
                                                    <Link to="/"
                                                          className="d-flex align-items-center text-decoration-none">
                                                        <img src="/images/arrow-bright-long.svg" alt="no images"/>
                                                        Russian
                                                    </Link>
                                                </li>
                                                <li className="">
                                                    <Link to="/"
                                                          className="d-flex align-items-center text-decoration-none">
                                                        <img src="/images/arrow-bright-long.svg" alt="no images"/>
                                                        Uzbek
                                                    </Link>
                                                </li>
                                                <li className="">
                                                    <Link to="/"
                                                          className="d-flex align-items-center text-decoration-none">
                                                        <img src="/images/arrow-bright-long.svg" alt="no images"/>
                                                       Ispan
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Navbar;