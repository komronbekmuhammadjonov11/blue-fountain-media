import React from 'react';
import "../../style/home/footer.scss";
import {Link} from "react-router-dom";

function Footer(props) {
    let a;
    return (
        <div className="footer">
            <div className="footer-top">
                <div className="row">
                    <div className="col-md-4">
                        <h2>New York Headquarters</h2>
                        <p>
                            102 Madison Avenue - Second Floor
                            New York, NY 10016 <br/>
                            T: (212) 260.1978
                        </p>
                        <span className="animation-button">
                            <Link to="/contact" className="button-text d-flex align-items-center text-decoration-none">
                                <span>Biz bilan aloqa qiling</span>
                                <button>
                                    <img src="/images/arrow-white-double.svg" alt="no images"/>
                                </button>
                            </Link>
                        </span>
                    </div>
                    <div className="col-md-4">
                        <h2>
                            Ijtimoiy tarmoqlarimizga qo'shiling
                        </h2>
                        <div className="icon-group">
                            <Link className="icon linkedink"></Link>
                            <a className="icon facebook"></a>
                            <a className="icon instagram"></a>
                            <a className="icon twitter"></a>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <h2>
                            Keling, keyin nima bo'lishini muhokama qilaylik
                        </h2>
                        <p>
                            Loyiha yoki savolingiz bormi? <br/>
                            Sizdan eshitishni istardik.
                        </p>
                        <span className="animation-button">
                            <Link to="/contact" className="button-text d-flex align-items-center text-decoration-none">
                                <span>Biz bilan aloqa qiling</span>
                                <button>
                                    <img src="/images/arrow-white-double.svg" alt="no images"/>
                                </button>
                            </Link>
                        </span>
                    </div>
                </div>
            </div>
            <div className="footer-bottom">
                <div className="text d-flex justify-content-between align-items-center">
                    <p className="mb-0">© 2021 Team Plus. All rights reserved</p>
                    <Link to="/">Privacy Policy</Link>
                </div>
            </div>
        </div>
    );
}

export default Footer;