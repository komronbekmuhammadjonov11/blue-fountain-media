import React from 'react';
import "../../style/home/myCompanyPage.scss";
import {Link} from "react-router-dom";

function MyComponyPage(props) {
    return (
       <div className="my-company-content-1">
           <div className="my-company-content">
               <div className="img">
                   <img
                       src="https://www.bluefountainmedia.com/sites/default/files/styles/homepage_hero_bottom_470x470_/public/2021-05/PowerPoint_image1.webp?itok=CVWHZixK"
                       alt="no images"/>
               </div>
               <div className="my-company-1">
                   <div className="my-company">
                       <div className="dark-blue-circle"></div>
                       <div className="blue-circle"></div>
                       <div className="title">
                           #Team+
                       </div>
                       <div className="red-line"></div>
                       <div className="text-content">
                           <h2>Bizga qo'shilishni xohlaysizmi?</h2>
                           <p>Biz tasavvur va texnologiyani birlashtiradigan  tajribali
                               dasturchilarni yaratuvchi iqtidorli global jamoamiz. Biz ishga olamiz.</p>
                           <div className="buttons">
                               <span className="animation-button">
                                    <Link to="/contact" className="button-text d-flex align-items-center text-decoration-none">
                                        <span>Biz bilan aloqa qiling</span>
                                        <button>
                                            <img src="/images/arrow-white-double.svg" alt="no images"/>
                                        </button>
                                    </Link>
                                </span>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
    );
}

export default MyComponyPage;