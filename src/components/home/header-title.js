import React from 'react';

function HeaderTitle(props) {
    return (
        <div className="header-title">
            <h1>Company</h1>
            <div className="red-line">

            </div>
            <h2 className="leder-ship">
                Leader
            </h2>
            <p>Jamoani ruxlantirish. Ularga mativatsiya berish.
                Jamoani yuqori cho'qilarga olib chiqish eng
                kotta maqsad.</p>
        </div>
    );
}

export default HeaderTitle;