import React from 'react';
import Navbar from "../home/navbar";
import "../../style/contact/contact.scss";
import {Link} from "react-router-dom";
import Footer from "../home/footer";

function Contact(props) {
    React.useEffect(()=>{
        window.scrollTo(0,0);
    });
    return (
        <div className="contact">
            <div className="right-bg">
                <img src="/images/dots-arrow-right-side-large.png" alt="no images"/>
            </div>
            <div className="blue-design">
                <div className="blue-circle"></div>
                <div className="white-border"></div>
                <div className="red-border"></div>
            </div>
            <Navbar/>
            <div className="contact-content">
                <h1>Aloqa </h1>
                <div className="red-line"></div>
               <div className="main-content">
                   <div className="row">
                       <div className="col-md-6">
                           <h3>Bizga murojat qiling</h3>
                           <div className="input-group">
                               <div className="input-content">
                                   <input type="text"  placeholder="Ismingiz" />
                               </div>
                               <div className="input-content">
                                   <input type="text" placeholder="Familyangiz" />
                               </div>
                           </div>
                           <div className="input-group">
                               <div className="input-content">
                                   <input type="email" placeholder="Email" />
                               </div>
                               <div className="input-content">
                                   <input type="tel" placeholder="Telefon  raqam"/>
                               </div>
                           </div>
                           <div className="company-name-input">
                               <div className="input-content">
                                   <input type="text" placeholder="Company Name" />
                               </div>
                           </div>
                           <div className="text-area">
                               <div className="input-content">
                                   <textarea placeholder="Murojat matni"/>
                               </div>
                           </div>
                           <div className="add">
                               <span className="animation-button">
                                    <Link to="/contact" className="button-text d-flex align-items-center text-decoration-none">
                                        <span>Biz bilan aloqa qiling</span>
                                        <button>
                                            <img src="/images/arrow-white-double.svg" alt="no images"/>
                                        </button>
                                    </Link>
                                </span>
                           </div>
                       </div>
                       <div className="col-md-6">
                            <div className="bg-darknow">
                                <div className="title">
                                    <div className="title-left">
                                       <span> TeamPlus</span>
                                        <div className="red-line"></div>
                                    </div>
                                    <div className="title-right">
                                        <a  href="tel:212  260.1978" className="tel d-none text-decoration-none">(212) 260.1978</a>
                                       <span> VISIT BFM</span>
                                        <div className="red-line"></div>
                                    </div>
                                </div>
                                <div className="contact-main">
                                    <a href="tel:212  260.1978" className="contact-main-left text-decoration-none">
                                        19.10.2021
                                    </a>
                                    <div className="contact-main-right">
                                        <span>
                                            TeamPlus conpanyasini maqsadi
                                            katta projectlar orqali jahon bozorlariga
                                            chiqish va  yoshlarga ta’lim berish.
                                        </span>
                                        <p>102 Madison Avenue, Second Floor
                                            New York, NY 10016</p>
                                        <Link to="/">GET DIRECTIONS</Link>
                                    </div>
                                </div>
                                <div className="footer-text">
                                    OTHER OFFICES
                                </div>
                                <div className="red-line aa"></div>
                                <div className="footer-1">
                                    <div className="footer-1-left">
                                        <div className="titlee">Seattle Office</div>
                                        <p>14980 NE 31st Way, Suite <br/> 120 <br/>
                                            Redmond, WA 98052</p>
                                        <Link to="/">
                                            GET DIRECTIONS
                                        </Link>
                                    </div>
                                    <div className="footer-1-right">
                                        <div className="titlee">Chicago Office</div>
                                        <p>500 W Madison St, #1610 <br/>
                                            Chicago, IL 60661</p>
                                        <Link to="/">
                                            GET DIRECTIONS
                                        </Link>
                                    </div>
                                </div>
                            </div>
                       </div>
                   </div>
               </div>
            </div>
            <Footer/>
        </div>
    );
}

export default Contact;