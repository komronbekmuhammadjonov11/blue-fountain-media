import React from 'react';
import "../../style/company/manager.scss";
function Manager(props) {
    return (
        <div className="manager">
            <div className="bg-content">
                <div className="dashed-line"></div>
                <div className="row">
                    <div className="col-sm-6 d-none">
                        <div className="blue-circle">
                            <img
                                src="https://www.bluefountainmedia.com/sites/default/files/styles/feature_leader_475x475/public/2019-10/Brian-Byer-2.webp?itok=jhZydXuM"
                                alt="no images"/>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="text-content">
                            <div className="manager-text">
                                General manager
                            </div>
                            <h2>Brian Byer</h2>
                            <div className="text">Brian is the General Manager for Blue Fountain Media. He joined Blue Fountain Media in
                                2013, bringing over 20 years of operations, technology, marketing, management and
                                business analysis expertise. Brian manages all agency activities relating to the
                                production Content & Commerce websites and facilitates the growth of our technology
                                partnerships. The production teams Brian leads are as diverse as the needs of BFM’s
                                clients and he thrives on ensuring our clients are consistently delighted by the work
                                our agency produces.</div>
                            <div className="text">Brian received his degree in Economics from the University of Colorado, Boulder. Brian is
                                a frequent contributor to the press, including: CMSwire, MarketingProfs, Martech Advisor
                                and Entrepreneur.</div>
                            <div className="text">Brian received his degree in Economics from the University of Colorado, Boulder. Brian is
                                a frequent contributor to the press, including: CMSwire, MarketingProfs, Martech Advisor
                                and Entrepreneur.</div>
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="blue-circle">
                            <img
                                src="https://www.bluefountainmedia.com/sites/default/files/styles/feature_leader_475x475/public/2019-10/Brian-Byer-2.webp?itok=jhZydXuM"
                                alt="no images"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Manager;