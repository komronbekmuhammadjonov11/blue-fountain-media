import React from 'react';
import Navbar from "../home/navbar";
import HeaderTitle from "../home/header-title";
import "../../style/company/header.scss";
function Header(props) {
    return (
        <div className="header">
            <div className="bg-red-line"></div>
            <div className="bg-white-line"></div>
            <Navbar/>
            <HeaderTitle/>
        </div>
    );
}

export default Header;