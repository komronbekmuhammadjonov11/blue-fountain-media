import React from 'react';
import "../../style/company/heads.scss";
function Heads(props) {
    return (
        <div className="heads">
            <div className="red-line"></div>
            <h2>Backend team</h2>
            <p>Ular bizning backend dasturchilarimiz. Web sayt himoyasi uchun kuchli mutahasislar  </p>
            <div className="row">
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="head-img">
                                <img src="/images/Tatyana-Khamdamova.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="head-name">
                            <span>Tatyana Khamdamova</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="head-title">
                            Head of Design
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                <div className="card">
                    <div className="img-content">
                        <div className="line"></div>
                        <div className="grey-circle">
                        </div>
                        <div className="head-img">
                            <img src="/images/Bryn-Dodson.png" alt="no images"/>
                        </div>
                    </div>
                    <div className="head-name">
                        <span>Bryn Dodson</span>
                    </div>
                    <div className="red-line"></div>
                    <div className="head-title">
                        Director of Content
                    </div>
                </div>
            </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="head-img">
                                <img src="/images/John-Marcinuk-2.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="head-name">
                            <span>John Marcinuk</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="head-title">
                            Head of Marketing
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="head-img">
                                <img src="/images/Dan-Drapeau.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="head-name">
                            <span>Dan Drapeau</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="head-title">
                            Head of Technologya
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="head-img">
                                <img src="/images/Britney-Wallace.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="head-name">
                            <span>Britney Wallace</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="head-title">
                            Head of User Experience
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="head-img">
                                <img src="/images/James-McCrae.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="head-name">
                            <span>James McCrae
                                                </span>
                        </div>
                        <div className="red-line"></div>
                        <div className="head-title">
                            Director, BFM Brand Lab
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Heads;