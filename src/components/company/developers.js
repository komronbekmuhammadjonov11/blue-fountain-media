import React from 'react';
import "../../style/home/developers.scss";
function Developers(props) {
    return (
        <div className="developers">
            <div className="row">
                <div className="col-md-4">
                    <img src="/images/revision-image-1.jpg" alt="no images"/>
                    <div className="white"></div>
                    <div className="content">
                        <div className="count">
                            100
                        </div>
                        <div className="text">
                            Front End
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <img src="/images/revision-image-2.jpg" alt="no images"/>
                    <div className="white"></div>
                    <div className="content">
                        <div className="count">
                            200
                        </div>
                        <div className="text">
                            Back End
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <img src="/images/revision-image-3.jpg" alt="no images"/>
                    <div className="white"></div>
                   <div className="content">
                       <div className="count">
                           300
                       </div>
                       <div className="text">
                           Full Stack
                       </div>
                   </div>
                </div>
            </div>
        </div>
    );
}

export default Developers;