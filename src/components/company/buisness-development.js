import React from 'react';
import "../../style/company/buisness-development.scss";
function BusinessDevelopment(props) {
    return (
        <div className="business-development">
            <div className="right-arrow-background">
                <img src="/images/dots-arrow-right-side-large.png" alt="no images"/>
            </div>
            <div className="red-line"></div>
            <h2>Team leaders
            </h2>
            <p>Jamoaga yo'l boshchilik qilayotgan leaderlar</p>
            <div className="row">
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="development-img">
                                <img src="/images/leadership_JeffAaron.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="development-name">
                            <span>Jeff Aaron</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="development-title">
                            Director, Business Development
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="development-img">
                                <img src="/images/MattSmith.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="development-name">
                           <span> Matt Smith</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="development-title">
                            Director, Business Development
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default BusinessDevelopment;