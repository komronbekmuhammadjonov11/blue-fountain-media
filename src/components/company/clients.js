import React from 'react';
import "../../style/company/clients.scss";
function Clients(props) {
    return (
        <div className="clients">
            <div className="background-left">
                <img src="/images/dots-arrow-left-side.png" alt="no images"/>
            </div>
            <div className="red-line"></div>
            <h2>Frontend team
            </h2>
            <p>Ular bizning ajoyib web sayt dizaynerlari. Ularning mahorati bilan <br/> ko'plab saytlar yaralmoqda. </p>
            <div className="row">
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="client-img">
                                <img src="/images/Seb-Lyner.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="client-name">
                            <span>Sebastian Lyner</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="client-title">
                            VP, Agency Production
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="client-img">
                                <img src="/images/Jenn-McCoy.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="client-name">
                            <span> Jenn McCoy</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="client-title">
                            VP, Client Services
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="client-img">
                                <img src="/images/Josh-Werkstell.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="client-name">
                            <span> Josh Werkstell</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="client-title">
                            Senior Account Director
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="client-img">
                                <img src="/images/John-Renaldo.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="client-name">
                            <span>John Renaldo</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="client-title">
                            Senior Account Director
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="client-img">
                                <img src="/images/Brynn-Smith-Raska.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="client-name">
                            <span> Brynn Smith-Raska</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="client-title">
                            Senior Account Director
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="client-img">
                                <img src="/images/leadership_LisaPowys.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="client-name">
                            <span> Lisa Powys</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="client-title">
                            Account Director
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="client-img">
                                <img src="/images/Kevin-Sergo.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="client-name">
                            <span> Kevin Sergo</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="client-title">
                            Account Director
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Clients;