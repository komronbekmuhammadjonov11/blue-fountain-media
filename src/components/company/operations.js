import React from 'react';
import "../../style/company/operations.scss";
function Operations(props) {
    return (
        <div className="operations">
            <div className="bg-dashed-line"></div>
            <div className="red-line"></div>
            <h2>Design team</h2>
            <p>Ular bizning juda kuchli grafik dizaynerlarimiz.
                Ularning yuragi bizning saytlarda uradi  </p>
            <div className="row">
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="operation-img">
                                <img src="/images/Samantha-Lambert.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="operation-name">
                            <span>Samantha Lambert</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="operation-title">
                            Director of Human Resources
                        </div>
                    </div>
                </div>
                <div className="col-sm-4">
                    <div className="card">
                        <div className="img-content">
                            <div className="line"></div>
                            <div className="grey-circle">
                            </div>
                            <div className="operation-img">
                                <img src="/images/Thomas-Duffy.png" alt="no images"/>
                            </div>
                        </div>
                        <div className="operation-name">
                            <span>Thomas Duffy</span>
                        </div>
                        <div className="red-line"></div>
                        <div className="operation-title">
                            Talent Acquisition Manager
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Operations;