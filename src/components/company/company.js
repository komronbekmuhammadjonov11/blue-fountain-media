import React from 'react';
import Header from "./header";
import Manager from "./manager";
import BusinessDevelopment from "./buisness-development";
import Clients from "./clients";
import Heads from "./heads";
import Operations from "./operations";
import Footer from "../home/footer";
import "../../css/leader.css";
function Company(props) {
    React.useEffect(()=>{
        window.scrollTo(0,0);
    });
    return (
        <div className="leader">
            <Header/>
            <Manager/>
            <BusinessDevelopment/>
            <Clients/>
            <Heads/>
            <Operations/>
            <Footer/>
        </div>
    );
}

export default Company;