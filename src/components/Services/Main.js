import React , {useState} from "react";
import ServicesTitile from "./servicesTitile"
import ViewsEven from "./viewsEven";
import ViewsOld from "./viewsOld";
import "../../style/services/main.scss"


function Main (){
 const [control, setControl] = useState(true)

    return(
        <div className = 'main'>

            <div className = "right-png">
                <img src= "/images/dots-arrow-right-side-large.png"></img>
            </div>
            <div className = "left-png1">
                <img src= "/images/dots-arrow-left-side.png"></img>
            </div>
            <div className = "left-png2">
                <img src= "/images/dots-arrow-left-side.png"></img>
            </div>
          <ServicesTitile/>
            <div className = "bel">
                <div className = "row">
                    <ViewsEven/>
                    <ViewsOld/>
                    <ViewsEven/>
                    <ViewsOld/>
                    <ViewsEven/>
                </div>
              
            </div>          

             {/* <div className = "slide-text">
             <div className = "slide-content1">
                        <div className="content1-in">
                            <div class="blue-circle1"></div>
                            <div class="blue-dark-circle1"></div>
                            <div class="grey-hoop1"></div>
                            <div class="red-hoop1"></div>
                            <div className = 'image1'>
                            <img src={control ? "https://www.bluefountainmedia.com/sites/default/files/styles/slideshow_740x740/public/2018-08/FabioRedivo.webp?itok=WBlPmtaG" : "https://www.bluefountainmedia.com/sites/default/files/styles/slideshow_740x740/public/2018-08/BrynDodson.webp?itok=OS7rb_yE"}></img>
                            </div>
                        </div>
                   </div>
                <div className = "ServicesTitle"> 
            
                <div className = "min-big">
                                <button className = "prev" onClick = {(e)=>{setControl(!control)}}><h1><p>&lt;</p></h1></button>
                            <button className = "next" onClick = {(e)=>{setControl(!control)}}><h1><p>&gt;</p></h1></button>
                            
                        
                <div className = "min-alll">
                    <div className ="content">
                        
                            <div className = "left">
                                <h1 className = "page-titile">MEET OUR EXPERTS</h1>
                                <div className ="red-bar"></div>
                                <h1>&#10077;</h1>
                                <p className = "title-text">{control ? "You're only as good as your latest design." : "To me, BFM stands out for its collegiality and the drive to push projects forward. You’re always in a team of incredibly talented people, and it’s rare for us to leave a room without having solved at least one problem."}</p>
                                <h2 >
                                    <strong> {control ? "Fabio Redivo" : "Bryn Dodson"}</strong>
                                    
                                </h2>
                                <h2 className = "position">{control ? "Senior Brand Director" : "Director of Content"}</h2>
                                
                                
                            </div>
                            
                                    <div class="right-image-wrapper">
                                        <div className = "top-left-big">
                                        <div class="top-left-grey-hoop"></div>
                                        </div>
                                        
                                                
                                        <div class="blue-circle"></div>
                                        <div class="blue-dark-circle"></div>
                                        <div class="grey-hoop"></div>
                                        <div class="red-hoop"></div>
                                        <div className = 'image'>
                                        <img src={control ? "https://www.bluefountainmedia.com/sites/default/files/styles/slideshow_740x740/public/2018-08/FabioRedivo.webp?itok=WBlPmtaG" : "https://www.bluefountainmedia.com/sites/default/files/styles/slideshow_740x740/public/2018-08/BrynDodson.webp?itok=OS7rb_yE"}></img>
                                        </div>
                                            
                                        
                                    </div>
                            

                            
                    
                    
                    
                    </div>
                    
                </div>
            </div>
            
                </div>

            </div> */}
        </div>
    )
}

export default Main;