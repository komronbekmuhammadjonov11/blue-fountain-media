import React from "react";
import "../../style/servicesTitle.scss"
import TitleImg from "../../images/services_banner_main.jpg"
function ServicesTitle(props){
    return(
        <div className = "ServicesTitle">
            <div className = "min-all">
                <div className ="content">
                    
                        <div className = "left">
                            <h1 className = "page-titile">Services</h1>
                            <div className ="red-bar"></div>
                            <p className = "title-text">Blue Fountain Media brands, builds, and promotes, offering end-to-end solutions, agility in execution, and depth of specialization.</p>
                        </div>
                        
                        <div class="right-image-wrapper">
                            <div className = "top-left-big">
                            <div class="top-left-grey-hoop"></div>
                            </div>
                            <div class="blue-circle"></div>
                            <div class="grey-hoop"></div>
                            <div class="red-hoop"></div>
                            <div className = 'image-titile'>
                               <div className = 'image-titile2'>
                                    {/* <img  src = {TitleImg} className = "in-img"  /> */}
                                </div>
                            </div>
                        </div>
                    
                
                
                
                </div>
            </div>
        </div>
    )
}
export default ServicesTitle;