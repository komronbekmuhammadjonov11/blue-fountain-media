import React from "react";
import "../../style/services/servicesInfo.scss"
import experience from "../../images/category-experience.png"
function ServicesInfo(props) {
    return (
        <div className="ServicesInfo">
            <div className="services-icon">
                <div className="icon">
                    <img src={experience}/>
                </div>
                <div className="red-bar"></div>
                </div>
                    <div className="services-content">
                    <h2 className = "h2">Experience Design</h2>
                    <p>A digital platform’s strategy, design, and development must all promote one goal: creating a valuable, memorable experience for the user. We create experiences that connect deeply with your target audience’s needs, and inspire not just affection, but action.</p>
            
                </div>
                    <div class="col-md-8">
                        <div className ="circle-animate-menu">
                            <div class="tab-list">
                                <ul class="list-unstyled">
                                    <li class="">
                                        <a class="d-flex align-items-center text-decoration-none" href="/">
                                            <img src ="../../images/arrow-bright-long.svg" alt="no images" /> 
                                            <p>Digital Strategy</p> 
                                        </a>
                                        <div className ="after"></div>
                                    </li>
                                    <li class="">
                                        <a class="d-flex align-items-center text-decoration-none" href="/">
                                            <img src ="../../images/arrow-bright-long.svg" alt="no images" /> 
                                            <p>Digital Strategy</p> 
                                        </a>
                                        <div className ="after"></div>
                                    </li>
                                    <li class="">
                                        <a class="d-flex align-items-center text-decoration-none" href="/">
                                            <img src ="../../images/arrow-bright-long.svg" alt="no images" /> 
                                            <p>Digital Strategy</p> 
                                        </a>
                                        <div className ="after"></div>
                                    </li>
                                    <li class="">
                                        <a class="d-flex align-items-center text-decoration-none" href="/">
                                            <img src ="../../images/arrow-bright-long.svg" alt="no images" /> 
                                            <p>Digital Strategy</p> 
                                        </a>
                                        <div className ="after"></div>
                                    </li>
                                
                                </ul>
                            </div>
                        </div>
                    </div>
        </div>
    )
}
export default ServicesInfo;