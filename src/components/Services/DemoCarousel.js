import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import "../../style/services/demoCarousel"
class DemoCarousel extends Component {
    render() {
        return (
            <Carousel  >
             <div className = "min-all">
                <div className ="content">
                    
                        <div className = "left">
                            <h1 className = "page-titile">MEET OUR EXPERTS</h1>
                            <div className ="red-bar"></div>
                            <h1>&#10077;</h1>
                            <p className = "title-text">You're only as good as your latest design.</p>
                            <h2 >
                                <strong>Fabio Redivo</strong>
                                
                            </h2>
                            <h2 className = "position">Senior Brand Director</h2>
                            
                            
                        </div>
                        
                        <div class="right-image-wrapper">
                            <div className = "top-left-big">
                            <div class="top-left-grey-hoop"></div>
                            </div>
                            <div class="blue-circle"></div>
                            <div class="blue-dark-circle"></div>
                            <div class="grey-hoop"></div>
                            <div class="red-hoop"></div>
                            <div className = 'image'>
                               <img src="https://www.bluefountainmedia.com/sites/default/files/styles/slideshow_740x740/public/2018-08/FabioRedivo.webp?itok=WBlPmtaG"></img>
                            </div>
                        </div>
                    
                
                
                
                </div>
            </div>
            <div className = "min-all">
                <div className ="content">
                    
                        <div className = "left">
                            <h1 className = "page-titile">MEET OUR EXPERTS</h1>
                            <div className ="red-bar"></div>
                            <h1>&#10077;</h1>
                            <p className = "title-text">You're only as good as your latest design.</p>
                            <h2 >
                                <strong>Fabio Redivo</strong>
                                
                            </h2>
                            <h2 className = "position">Senior Brand Director</h2>
                            
                            
                        </div>
                        
                        <div class="right-image-wrapper">
                            <div className = "top-left-big">
                            <div class="top-left-grey-hoop"></div>
                            </div>
                            <div class="blue-circle"></div>
                            <div class="blue-dark-circle"></div>
                            <div class="grey-hoop"></div>
                            <div class="red-hoop"></div>
                            <div className = 'image'>
                               <img src="https://www.bluefountainmedia.com/sites/default/files/styles/slideshow_740x740/public/2018-08/FabioRedivo.webp?itok=WBlPmtaG"></img>
                            </div>
                        </div>
                    
                
                
                
                </div>
            </div>
            <div className = "min-all">
                <div className ="content">
                    
                        <div className = "left">
                            <h1 className = "page-titile">MEET OUR EXPERTS</h1>
                            <div className ="red-bar"></div>
                            <h1>&#10077;</h1>
                            <p className = "title-text">You're only as good as your latest design.</p>
                            <h2 >
                                <strong>Fabio Redivo</strong>
                                
                            </h2>
                            <h2 className = "position">Senior Brand Director</h2>
                            
                            
                        </div>
                        
                        <div class="right-image-wrapper">
                            <div className = "top-left-big">
                            <div class="top-left-grey-hoop"></div>
                            </div>
                            <div class="blue-circle"></div>
                            <div class="blue-dark-circle"></div>
                            <div class="grey-hoop"></div>
                            <div class="red-hoop"></div>
                            <div className = 'image'>
                               <img src="https://www.bluefountainmedia.com/sites/default/files/styles/slideshow_740x740/public/2018-08/FabioRedivo.webp?itok=WBlPmtaG"></img>
                            </div>
                        </div>
                    
                
                
                
                </div>
            </div>
            <div className = "min-all">
                <div className ="content">
                    
                        <div className = "left">
                            <h1 className = "page-titile">MEET OUR EXPERTS</h1>
                            <div className ="red-bar"></div>
                            <h1>&#10077;</h1>
                            <p className = "title-text">You're only as good as your latest design.</p>
                            <h2 >
                                <strong>Fabio Redivo</strong>
                                
                            </h2>
                            <h2 className = "position">Senior Brand Director</h2>
                            
                            
                        </div>
                        
                        <div class="right-image-wrapper">
                            <div className = "top-left-big">
                            <div class="top-left-grey-hoop"></div>
                            </div>
                            <div class="blue-circle"></div>
                            <div class="blue-dark-circle"></div>
                            <div class="grey-hoop"></div>
                            <div class="red-hoop"></div>
                            <div className = 'image'>
                               <img src="https://www.bluefountainmedia.com/sites/default/files/styles/slideshow_740x740/public/2018-08/FabioRedivo.webp?itok=WBlPmtaG"></img>
                            </div>
                        </div>
                    
                
                
                
                </div>
            </div>
            
            
             </Carousel>
        );
    }
}

ReactDOM.render(<DemoCarousel />, document.querySelector('.demo-carousel'));
