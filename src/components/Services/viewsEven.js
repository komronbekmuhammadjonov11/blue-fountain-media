import React from "react";
import ServicesInfo from "./ServicesInfo";
import ImageServices from "./ImageServices";

import "../../style/services/viewsEven.scss"

function ViewsEven(){
    return(
        <div className = "viewsEven">
            
            <div className = "services-image">
                <ImageServices/>
            </div>

            <div className = "services-body">
                <ServicesInfo/>
            </div>
            
        </div>
    )
}
export default ViewsEven;