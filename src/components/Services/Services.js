import React from "react";
import Main from "./Main";
import "../../style/services/services.scss"
import Footer from "../home/footer"
import Navbar from "../home/navbar"
function Services(props){
    React.useEffect(()=>{
        window.scrollTo(0,0)
    }, []);
    return(
        <>
            <Navbar/>
            <div className = "white-content-background-1">
                <Main/>
            </div>
            <Footer/>
        </>
    );

}
export default Services;