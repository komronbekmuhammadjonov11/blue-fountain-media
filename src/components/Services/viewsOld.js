import React from "react";
import ServicesInfo from "./ServicesInfo";
import ImageServices from "./ImageServices";

import "../../style/services/viewsOld.scss"

function ViewsOld (){
    return(
        <div className = "viewsOld">
            
            <div className = "views-image">
                <ImageServices/>
            </div>
            <div className = "views-body">
                <ServicesInfo/>
            </div>
            
        </div>
    )
}
export default ViewsOld;