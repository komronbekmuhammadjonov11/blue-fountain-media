import React from 'react';
import Navbar from "../home/navbar";
import "../../style/work/work.scss";
import Portfolio from "../home/portfolio";
import Footer from "../home/footer";
function Work(props) {
    React.useEffect(()=>{
        window.scrollTo(0,0);
    });
    function showVideo() {
        let video=document.querySelector('.video-player');
        let bg_img=document.querySelector('.bg-img');
        bg_img.style.display="none";
        video.classList.add('active');
        video.play();
        console.log(video);
    }
    return (
        <div className="work">
            <Navbar/>
            <div className="video-content">
                <div className="bg-img">
                    <div className="text-content">
                        <h1 className="">Bizning ishni jamoamiz bilan birgalikda bajaramiz. <br/>
                            Katta loyhalarni qiziqish bilan boshlaymiz. </h1>
                        <button onClick={showVideo}></button>
                    </div>
                </div>
                <video className="video-player"
                       width={"100%"}
                       height={"100%"}
                       src="https://www.bluefountainmedia.com/sites/default/files/2019-04/BFM%20Project%20Reel%202018.mp4"
                       id="banner-video"
                       controls
                />
            </div>
            <Portfolio/>
            <Footer/>
        </div>
    );
}

export default Work;