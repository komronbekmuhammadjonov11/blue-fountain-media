import React from 'react'
import "./css/App.css"
import Company from "./components/company/company";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Home from "./components/home/home";
import Contact from "./components/contact/contact";
import Work from "./components/work/work";
import Services from "./components/Services/Services";
function App() {
  return (
      <div className="App">
          <BrowserRouter>
              <Switch>
                  <Route path="/company/leadership" exact component={Company}/>
                  <Route path ="/services" exact component = {Services}/>
                  <Route path="/contact" exact component={Contact}/>
                  <Route path="/work" exact component={Work}/>
                  <Route path="/" exact component={Home}/>
              </Switch>
          </BrowserRouter>
      </div>
  );
}

export default App;
